# Lignum Plugin 

Implementation of the Lignum model into GroIMP. The LIGNUM is a generic functional-structural plant model (FSPM) designed and parameterized for
modelling coniferous as well as deciduous tree species.
