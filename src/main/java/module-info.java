module lignum {
	exports de.grogra.lignum.stlVoxelspace;
	exports de.grogra.lignum.jadt;
	exports de.grogra.lignum.sky;
	exports de.grogra.lignum.stlLignum;

	requires graph;
	requires imp;
	requires imp3d;
	requires java.datatransfer;
	requires java.desktop;
	requires m3DCS;
	requires math;
	requires platform;
	requires platform.core;
	requires raytracer;
	requires rgg;
	requires utilities;
	requires vecmath;
	requires xl;
	requires xl.core;
}